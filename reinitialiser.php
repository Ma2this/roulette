<?php
		// Connexion à la base de données
		$bdd = new PDO('mysql:host=localhost;dbname=class', 'matthis', 'matthis');
		
		// Récupération de tous les noms dans la base de données
		$req = $bdd->query('SELECT nom FROM liste_noms');
		
		// Affichage des noms dans une liste
		while ($donnees = $req->fetch())
		{
			echo '<li>'.$donnees['nom'].'</li>';
		}
		
		// Fermeture de la requête et de la connexion à la base de données
		$req->closeCursor();
		$bdd = null;
		?>