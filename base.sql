CREATE DATABASE class;

USE class;

CREATE TABLE liste_noms (
  id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  nom VARCHAR(50) NOT NULL
);

INSERT INTO liste_noms (nom) VALUES ('Matthis');
INSERT INTO liste_noms (nom) VALUES ('Antonin');
INSERT INTO liste_noms (nom) VALUES ('Luca');
INSERT INTO liste_noms (nom) VALUES ('Bastien');
INSERT INTO liste_noms (nom) VALUES ('Lucas');
INSERT INTO liste_noms (nom) VALUES ('Thomas');
INSERT INTO liste_noms (nom) VALUES ('Théo');
INSERT INTO liste_noms (nom) VALUES ('Thibault');

-----------------------------------------------------------

-- Création de la base de données
CREATE DATABASE class;

-- Sélection de la base de données
USE class;

-- Création de la table liste_noms
CREATE TABLE liste_noms (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL
);

-- Insertion de quelques noms dans la table
INSERT INTO liste_noms (nom) VALUES
    ('Alice'),
    ('Bob'),
    ('Charlie'),
    ('Dave'),
    ('Eve');
