<!DOCTYPE html>
<html>
<head>
	<title>Tirage au sort</title>
</head>
<body>

	<h1>Liste des personnes :</h1>
	<ul>
		<?php
		// Connexion à la base de données
		$bdd = new PDO('mysql:host=localhost;dbname=class', 'matthis', 'matthis');
		
		// Récupération de tous les noms dans la base de données
		$req = $bdd->query('SELECT nom FROM liste_noms');
		
		// Affichage des noms dans une liste
		while ($donnees = $req->fetch())
		{
			echo '<li>'.$donnees['nom'].'</li>';
		}
		
		// Fermeture de la requête et de la connexion à la base de données
		$req->closeCursor();
		$bdd = null;
		?>
	</ul>
	<button onclick="tirageAuSort()">Tirer au sort</button>
	<h2>Gagnants :</h2>
	<table id="gagnants">
		<tr>
			<th>Nom</th>
		</tr>
	</table>
	<button onclick="reinitialiser()">Réinitialiser la base de données</button>
	<script>
		function tirageAuSort() {
			// Envoi d'une requête Ajax pour récupérer un nom au hasard
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					// Ajout du nom dans le tableau des gagnants et suppression de la ligne correspondante dans la liste
					var nom = this.responseText;
					var gagnants = document.getElementById("gagnants");
					var ligne = gagnants.insertRow(-1);
					var cellule = ligne.insertCell(0);
					cellule.innerHTML = nom;
					var liste = document.getElementsByTagName("li");
					for (var i = 0; i < liste.length; i++) {
						if (liste[i].innerHTML == nom) {
							liste[i].parentNode.removeChild(liste[i]);
							break;
						}
					}
				}
			};
			xhr.open("GET", "tirage_au_sort.php", true);
			xhr.send();
		}
		
		function reinitialiser() {
			// Envoi d'une requête Ajax pour réinitialiser la base de données
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					// Rechargement de la page après la réinitialisation
					location.reload();
				}
			};
			// xhr.open("GET", "reinitialiser.php", true);
			// xhr.send();
		}
	</script>
</body>
</html>
